public class BasicClass
{
    private int i;
    private float f;

    public BasicClass()
    {
        i = 0;
        f = 0.0f;
    }

    public BasicClass(int ival, float fval)
    {
        i = ival;
        f = fval;
    }
}