import java.lang.Object;
import java.lang.reflect.*;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;

public class ObjectInspector
{
    private boolean recursive;
    private Vector<Object> processed;

    /**
     * Default no-argument constructor
     */
    public ObjectInspector()
    {
        recursive = false;
        processed = new Vector<Object>();
    }

    /**
     * Inspect the properties and fields of a given object 
     * @param obj An Object to inspect
     * @param recursive when set to true, will recursively inspect all objects within the object's fields.
     */
    public void inspect(Object obj, boolean recursive)
    {
        if(recursive)
        {
            this.recursive = true;
            if(!processed.contains(obj))
            {
                processed.add(obj);

                Class objectClass = obj.getClass();
                classInfo(obj, objectClass);
                System.out.println("\nFields: ");
                analyzeFields(obj, objectClass);
                //System.out.println("\nConstructors: ");
                //analyzeConstructors(obj, objectClass);
                //System.out.println("\nMethods: ");
                //analyzeMethods(obj, objectClass);
            }
        }
        else
        {
            this.recursive = false;

            Class objectClass = obj.getClass();
            classInfo(obj, objectClass);
            //System.out.println("\nFields: ");
            //analyzeFields(obj, objectClass);
            //System.out.println("\nConstructors: ");
            //analyzeConstructors(obj, objectClass);
            //System.out.println("\nMethods: ");
            //analyzeMethods(obj, objectClass);
        }
    }

    /**
     * Gets general information about the object's class (ie. superclass, implemented interfaces)
     */
    private void classInfo(Object obj, Class c)
    {
        String signature = Modifier.toString(c.getModifiers());
        signature += " class " + c.getSimpleName();
        if(c.getSuperclass() != null)
        {
            signature += "\n\textends " + c.getSuperclass().getName();
        }
        Class[] interfaces = c.getInterfaces();
        if(interfaces.length > 0)
        {
            signature += "\n\timplements ";
            for(int i = 0; i < interfaces.length; i++)
            {
                if(i == 0)
                {
                    signature += interfaces[i].getSimpleName();
                }
                else
                {
                    signature += ", " + interfaces[i].getSimpleName();
                }
            }
        }

        if(c.isArray() && c.getComponentType().isPrimitive())
        {
            int length = Array.getLength(obj);
            for(int i = 0; i < length; i++)
            {
                signature += "\nthis[" + i + "] = " + Array.get(obj, i);
            }
        }

        System.out.println(signature);
    }

    /**
     * Analyzes all the constructors available to an object
     */
    private void analyzeConstructors(Object obj, Class c)
    {
        Constructor[] constructors = c.getConstructors();
        
        for(int i = 0; i < constructors.length; i++)
        {
            analyzeConstructor(constructors[i]);
        }
    }

    /**
     * Helper method to analyzeConstructors
     */
    private void analyzeConstructor(Constructor c)
    {
        String signature = Modifier.toString(c.getModifiers());
        signature += " " + c.getName() + "(";

        Class[] parameters = c.getParameterTypes();

        for(int i = 0; i < parameters.length; i++)
        {
            if(i == 0)
            {
                signature += parameters[i].getSimpleName();
            }
            else
            {
                signature += ", " + parameters[i].getSimpleName();
            }
        }

        signature += ")";

        System.out.println(signature);
    }

    /**
     * Analyzes all the fields of a given object
     */
    private void analyzeFields(Object obj, Class c)
    {
        Queue<Field> fields = new LinkedList<Field>();
        
        Class current = c;

        while(current != null)
        {
            Field[] decl_fields = current.getDeclaredFields();
            int length = decl_fields.length;

            for(int i = 0; i < length; i++)
            {
                fields.add(decl_fields[i]);
            }

            current = current.getSuperclass();
        }

        while(!fields.isEmpty())
        {
            try
            {
                analyzeField(obj, fields.remove());
            }
            catch( IllegalAccessException e)
            {
                System.out.println("Unable to process field");
            }
        }
    }

    /**
     * Helper method to assist analyzeField in processing N-dimensional arrays
     */
    private void analyzeArray(String prefix, Object array)
    {
        // Basic check for an unallocated array
        if(array == null)
        {
            System.out.println(prefix + " = null");
        }
        // If an array of arrays, recurse
        else if(array.getClass().getComponentType().isArray())
        {
            int length = Array.getLength(array);
            for(int i = 0; i < length; i++)
            {
                analyzeArray(prefix + "[" + i + "]", Array.get(array, i));
            }
        }
        // If an array, but no more sub-arrays exist
        else if(array.getClass().isArray() && !array.getClass().getComponentType().isArray())
        {
            for(int i = 0; i < Array.getLength(array); i++)
            {
                // Get each object in the array
                Object obj = Array.get(array, i);

                // Check if it's null
                if(obj == null)
                {
                    System.out.println(prefix + " = null");
                }
                // Write value if it's primitive
                else if(array.getClass().getComponentType().isPrimitive())
                {
                    System.out.println(prefix + "[" + i + "] = " + obj);
                }
                // If it's an object and we're in recursive mode, inspect it
                else if(recursive)
                {
                    System.out.println(prefix + " = ");
                    System.out.println("=============RECURSIVELY INSPECTED OBJECT=============");
                    inspect(obj, recursive);
                    System.out.println("===========END RECURSIVELY INSPECTED OBJECT===========");
                }
                // If we aren't in recursive mode, just write the classname and hashcode
                else
                {
                    System.out.println(prefix + " = " + obj.getClass().getSimpleName() + " #" + obj.hashCode());
                }
            }
        }
    }

    /**
     * Helper method to analyzeFields
     */
    private void analyzeField(Object obj, Field f) throws IllegalAccessException
    {
        f.setAccessible(true);

        String signature = Modifier.toString(f.getModifiers()) + " " + f.getType().getSimpleName() + " " + f.getName();

        if(f.getType().isPrimitive())
        {
            signature += " = " + f.get(obj);
            System.out.println(signature);
        }
        else if(f.getType().isArray())
        {
            analyzeArray(signature, f.get(obj));
        }
        else if(f.get(obj) == null)
        {
            signature += " = null";
            System.out.println(signature);
        }
        else
        {
            if(!recursive)
            {
                signature += " = " + f.getType().getSimpleName() + " #" + f.get(obj).hashCode();
                System.out.println(signature);
            }
            else
            {
                signature += " = ";
                System.out.println(signature);
                System.out.println("=============RECURSIVELY INSPECTED OBJECT=============");
                inspect(f.get(obj), recursive);
                System.out.println("===========END RECURSIVELY INSPECTED OBJECT===========");
            }
            
        }
    }

    /**
     * Analyzes all Methods avaiable to the given object
     */
    private void analyzeMethods(Object obj, Class c)
    {
        // Create a queue for processing the individual methods
        Queue<Method> methods = new LinkedList<Method>();

        // Get all methods available to the class, including inherited Methods
        Class current = c;

        // Stop when there is no further superclass to query
        while(current != null)
        {
            int length = current.getDeclaredMethods().length;
            Method[] decl_methods = current.getDeclaredMethods();

            for(int i = 0; i < length; i++)
            {
                methods.add(decl_methods[i]);
            }

            current = current.getSuperclass();
        }

        // Analyze each individual method
        while(!methods.isEmpty())
        {
            analyzeMethod(methods.remove());
        }
    }

    /**
     *  Helper method to analyzeMethods
     */
    private void analyzeMethod(Method m)
    {
        // Get Modifiers
        String signature = Modifier.toString(m.getModifiers());
        // Get Return type
        signature += " " + m.getReturnType().getSimpleName();
        // Get Name
        signature += " " + m.getName() + "(";

        // Get Parameters
        Class[] parameters = m.getParameterTypes();
        for(int i = 0; i < parameters.length; i++)
        {
            if (i == 0)
            {
                signature += parameters[i].getSimpleName();
            }
            else
            {
                signature += ", " + parameters[i].getSimpleName();
            }
        }

        signature += ")";

        Class[] exceptions = m.getExceptionTypes();
        if(exceptions.length > 0)
        {
            signature += " throws";
            for(int i = 0; i < exceptions.length; i++)
            {
                signature += " " + exceptions[i].getSimpleName();
            }
        }
        System.out.println(signature);
    }
}