import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import org.jdom2.Document;

public class Reciever
{
    public static void main(String[] args)
    {
        if(args.length > 0)
        {
            try
            {
                ServerSocket socket = new ServerSocket(Integer.parseInt(args[0]));
                socket.setSoTimeout(250);
                System.out.println("Listening for connections on port " + args[0]);
                Object obj = null;
                Document doc = null;

                while(!socket.isClosed())
                {
                    try
                    {
                        Socket sock = socket.accept();
                        ObjectInputStream ois = new ObjectInputStream(sock.getInputStream());

                        doc = (Document) ois.readObject();

                        ois.close();
                        sock.close();
                        socket.close();
                    }
                    catch(IOException e)
                    {

                    }
                    catch(ClassNotFoundException e)
                    {
                        System.out.println("Cannot resolve type org.jdom2.Document");
                    }
                }

                obj = Deserializer.deserialize(doc);

                ObjectInspector inspector = new ObjectInspector();
                System.out.println("Inspecting deserialized object: ");
                inspector.inspect(obj, true);
            }
            catch(IOException e)
            {
                System.out.println("Failed to bind socket on port " + args[0]);
            }
        }
        else
        {
            System.out.println("Please provide a port to listen for connections");
        }
    }
}