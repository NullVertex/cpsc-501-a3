import java.lang.reflect.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Vector;
import org.jdom2.Document;
import org.jdom2.Element;

public class Deserializer
{
    public static Object deserialize(Document doc)
    {
        HashMap<Integer, Object> hashmap = new HashMap<Integer, Object>();

        Element root = doc.getRootElement();
        List<Element> objectElements = root.getChildren("object");

        while(!objectElements.isEmpty())
        {
            Element objectDOM = objectElements.remove(0);
            Object obj = null;
            try
            {
                String className = objectDOM.getAttribute("class").getValue();
                Class c = Class.forName(className);
                int id = objectDOM.getAttribute("id").getIntValue();

                //System.out.println("Attempting to deserialize object of type " + c.getName() + " with id " + id);
                
                if(c.isArray())
                {
                    int length = objectDOM.getAttribute("length").getIntValue();
                    obj = Array.newInstance(c.getComponentType(), length);
                    hashmap.put(id, obj);
                    //System.out.println("Hashmap[" + id + "] = new " + c.getSimpleName() + " of size " + length);

                    if(c.getComponentType().isPrimitive())
                    {
                        List<Element> arrayValues = objectDOM.getChildren("value");

                        for(int i = 0; i < arrayValues.size(); i++)
                        {
                            Element index = arrayValues.get(i);
                            Object valueAtIndex = parseToPrimitive(c.getComponentType(), index.getText());

                            Array.set(obj, i, valueAtIndex);
                        }
                    }
                    else
                    {
                        List<Element> arrayRefs = objectDOM.getChildren("reference");

                        for(int i = 0; i < arrayRefs.size(); i++)
                        {
                            Element index = arrayRefs.get(i);
                            Object objFromHashMap = hashmap.get(Integer.parseInt(index.getText()));
                            Array.set(obj, i, objFromHashMap);
                        }
                    }
                }
                else
                {
                    // get no argument Constructor
                    Constructor cns = c.getConstructor(new Class[] {});
                    cns.setAccessible(true);

                    // Create a new instance of the object with the no argument Constructor
                    obj = cns.newInstance();
                    // Store it in the HashMap
                    hashmap.put(id, obj);
                    //System.out.println("Hashmap[" + id + "] = new " + c.getSimpleName());

                    Queue<Field> fields = getClassFields(c);
                    while(!fields.isEmpty())
                    {
                        Field field = fields.remove();
                        field.setAccessible(true);
                        Element e = getDOMElement(objectDOM, field.getName());
                        String val = e.getChildText("value");
                        String ref = e.getChildText("reference");

                        //System.out.println("val = \"" + val + "\"\nref = \"" + ref + "\"");

                        if (val != null)
                        {
                            Object value = parseToPrimitive(field.getType(), val);
                            field.set(obj, value);
                        }
                        if(ref != null)
                        {
                            Object reference = hashmap.get(Integer.parseInt(ref));
                            if(reference != null)
                                field.set(obj, reference);
                            else
                                System.out.println("reference cannot be assigned as it does not exist in hash map");
                        }
                    }
                }

            }
            catch(ClassNotFoundException e)
            {
                System.out.println("Cannot find class with the specified name");
            }
            catch(NoSuchMethodException e)
            {
                System.out.println("Unable to reconstruct this object as it does not have a no-argument Constructor");
            }
            catch(Exception e)
            {

            }

            if(objectElements.isEmpty())
            {
                return obj;
            }
        }

        return null;
    }

    private static Object parseToPrimitive(Class c, String value)
    {
        Object obj = null;
        String type = c.getName();

        switch(type)
        {
            case "boolean":     return Boolean.parseBoolean(value);

            case "byte":        return Byte.parseByte(value);

            case "char":        return value.charAt(0);

            case "double":      return Double.parseDouble(value);

            case "float":       return Float.parseFloat(value);

            case "int":         return Integer.parseInt(value);

            case "long":        return Long.parseLong(value);

            case "short":       return Short.parseShort(value);
            
            default:            System.out.println("Unable to assign non-primitive data-type.");
                                break;
        }

        return obj;
    }

    private static String getReference(Element field)
    {
        try
        {
            return field.getChildText("reference");
        }
        catch(NullPointerException ex)
        {
            return null;
        }
    }

    private static String getValue(Element field)
    {
        try
        {
            return field.getChildText("value");
        }
        catch(NullPointerException e)
        {
            return null;
        }
    }

    private static Element getDOMElement(Element declaringObject, String fieldName)
    {
        List<Element> fieldElements = declaringObject.getChildren("field");
        while(!fieldElements.isEmpty())
        {
            Element current = fieldElements.remove(0);
            if (current.getAttributeValue("name").equals(fieldName))
                return current;
        }
        return null;
    }

    /**
     * Generates a linked list of all fields an object posseses, including superclass fields
     * @return linked list of all of an object's fields
     */
    private static LinkedList<Field> getClassFields(Class c)
    {
        LinkedList<Field> fields = new LinkedList<Field>();
        Class current = c;

        while(current != null)
        {
            Field[] decl_fields = current.getDeclaredFields();
            int length = decl_fields.length;

            for(int i = 0; i < length; i++)
            {
                fields.add(decl_fields[i]);
            }

            current = current.getSuperclass();
        }

        return fields;
    }
}