import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.InputMismatchException;
import java.util.Scanner;
import org.jdom2.Document;

public class ObjCreator
{
    public static void main(String[] args)
    {
        boolean done;
        Serializer serializer = new Serializer();
        Scanner scanner = new Scanner(System.in);
        int selection;
        Document doc = null;

        do
        {
            done = true;

            // Print Menu
            System.out.println("(0) Exit (Do nothing)");
            System.out.println("(1) Create a \'BasicClass\' object");
            System.out.println("(2) Create a String object");
            System.out.println("(3) Create a char[] object");
            System.out.println("(4) Create an object which references two BasicClass objects");

            System.out.print("\nEnter a selection (0 - 5): ");
            
            while(!scanner.hasNextInt())
            {
                scanner.next();
            }
            selection = scanner.nextInt();
            scanner.nextLine();

            switch(selection)
            {
                case 0:     break;

                case 1:     System.out.print("\nPlease enter an integer: ");
                            while(!scanner.hasNextInt())
                            {
                                scanner.next();
                            }
                            int ival = scanner.nextInt();
                            System.out.print("\nPlease enter a floating point value: ");
                            while(!scanner.hasNextFloat())
                            {
                                scanner.next();
                            }
                            float fval = scanner.nextFloat();
                            doc = serializer.serialize(new BasicClass(ival, fval));
                            break;
                
                case 2:     System.out.print("\nPlease enter some text: ");
                            String message = scanner.nextLine();
                            doc = serializer.serialize(message);
                            break;

                case 3:     System.out.print("\nPlease enter some text: ");
                            String contents = scanner.nextLine();
                            char[] charArray = contents.toCharArray();
                            doc = serializer.serialize(charArray);
                            break;

                case 4:     System.out.print("\nPlease enter an integer: ");
                            while(!scanner.hasNextInt())
                            {
                                scanner.next();
                            }
                            int valA = scanner.nextInt();
                            System.out.print("\nPlease enter a floating point value: ");
                            while(!scanner.hasNextFloat())
                            {
                                scanner.next();
                            }
                            float valB = scanner.nextFloat();
                            System.out.print("\nPlease enter an integer: ");
                            while(!scanner.hasNextInt())
                            {
                                scanner.next();
                            }
                            int valC = scanner.nextInt();
                            System.out.print("\nPlease enter a floating point value: ");
                            while(!scanner.hasNextFloat())
                            {
                                scanner.next();
                            }
                            float valD = scanner.nextFloat();
                            doc = serializer.serialize(new ReferenceObject(valA, valB, valC, valD));
                            break;

                default:    System.out.println("Please enter a valid selection.\n");
                            done = false;
                            break;
            }
        }
        while(!done);
        scanner.close();

        if(args.length > 0)
        {
            try
            {
                Socket socket = new Socket("localhost", Integer.parseInt(args[0]));
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

                serializer.write();
                if(doc != null)
                {
                    oos.writeObject(doc);
                    oos.close();
                    socket.close();

                    Object obj = Deserializer.deserialize(doc);
                    ObjectInspector inspector = new ObjectInspector();
                    System.out.println("");
                    inspector.inspect(obj, true);
                }
                    
            }
            catch(IOException e)
            {
                System.out.println("Unable to connect to reciever");
            }
        }
    }
}