public class ReferenceObject
{
    private BasicClass vara;
    private BasicClass varb;

    public ReferenceObject()
    {
        vara = new BasicClass();
        varb = new BasicClass();
    }

    public ReferenceObject(int a, float b, int c, float d)
    {
        vara = new BasicClass(a, b);
        varb = new BasicClass(c, d);
    }
}