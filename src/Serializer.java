import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.*;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class Serializer
{
    private Element root;
    private Document doc;
    private Vector<Object> processed;
    private String file;

    /**
     * Default no-argument constructor
     */
    public Serializer()
    {
        root = new Element("serialized");
        doc = new Document(root);
        processed = new Vector<Object>();
        file = "output.xml";
    }

    /**
     * Constructor allowing for a file name to be specified
     */
    public Serializer(String filename)
    {
        root = new Element("serialized");
        doc = new Document(root);
        processed = new Vector<Object>();
        file = filename;
    }

    /**
     * Serializes an object into the internal XML DOM
     * @return The internal DOM after the object has been serialized
     */
    public Document serialize(Object obj)
    {
        // Don't re-do objects
        if(!processed.contains(obj))
        {
            processed.add(obj);

            Element currentObj = new Element("object");
            currentObj.setAttribute("class", obj.getClass().getName());
            currentObj.setAttribute("id", Integer.toString(obj.hashCode()));

            // Serialize array values
            if(obj.getClass().isArray())
            {
                int arrayLength = Array.getLength(obj);
                currentObj.setAttribute("length", Integer.toString(arrayLength));

                // write "value" tags for arrays of primitives
                if(obj.getClass().getComponentType().isPrimitive())
                {
                    for(int i = 0; i < arrayLength; i++)
                    {
                        Element v = new Element("value");
                        v.setText(Array.get(obj, i).toString());

                        currentObj.addContent(v);
                    }
                }
                // write "reference" tags for arrays of objects
                else
                {
                    for(int i = 0; i < arrayLength; i++)
                    {
                        Element r = new Element("reference");
                        r.setText(Integer.toString(Array.get(obj, i).hashCode()));

                        currentObj.addContent(r);
                        serialize(Array.get(obj, i));
                    }
                }
            }

            // Serialize fields
            serializeFields(obj, currentObj);

            // add this object to the DOM root
            root.addContent(currentObj);
        }

        return doc;
    }

    /**
     * Generates a linked list of all fields an object posseses, including superclass fields
     * @return linked list of all of an object's fields
     */
    private LinkedList<Field> getFields(Object obj)
    {
        LinkedList<Field> fields = new LinkedList<Field>();
        Class current = obj.getClass();

        while(current != null)
        {
            Field[] decl_fields = current.getDeclaredFields();
            int length = decl_fields.length;

            for(int i = 0; i < length; i++)
            {
                fields.add(decl_fields[i]);
            }

            current = current.getSuperclass();
        }

        return fields;
    }

    /**
     * Serializes all of an object's fields into the DOM
     * @param obj the object being serialized
     * @param parent the object which "owns" these fields.
     */
    private void serializeFields(Object obj, Element parent)
    {
        // Treat the list of fields as a queue
        Queue<Field> fields = getFields(obj);

        // Process each field in the queue
        while(!fields.isEmpty())
        {
            Field currentField = fields.remove();
            currentField.setAccessible(true);

            // Create the field element
            Element f = new Element("field");
            f.setAttribute("name", currentField.getName());
            f.setAttribute("declaringclass", currentField.getDeclaringClass().getName());

            try
            {
                // Write values for primitives
                if(currentField.getType().isPrimitive())
                {
                    Element v = new Element("value");
                    v.setText(currentField.get(obj).toString());

                    f.addContent(v);
                }
                // Write a reference for non-primitives
                else if(currentField.get(obj) != null)
                {
                    Element r = new Element("reference");
                    r.setText(Integer.toString(currentField.get(obj).hashCode()));

                    f.addContent(r);
                    serialize(currentField.get(obj));
                }
            }
            catch(IllegalAccessException e)
            {
                System.out.println("Illegal field access");
            }

            // Add the field element to it's parent object
            parent.addContent(f);
        }
    }

    /**
     * Writes the XML document to a file
     */
    public void write() throws IOException
    {
        try
        {
            FileWriter writer = new FileWriter(file);
            XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat().setIndent("    "));
            outputter.output(doc, writer);
        }
        catch(IOException e)
        {
            throw new IOException();
        }
    }
}