.PHONY: default all run build tests docs clean

J=java
JC=javac
JD=javadoc
WD=$(shell pwd)
CLASSPATH="$(WD)/lib/junit.jar:$(WD)/lib/hamcrest-core.jar:$(WD)/lib/jdom.jar:$(WD)/lib/jdom-junit.jar:$(WD)/bin:$(WD)/src:$(WD)/tests"
JFLAGS=-cp $(CLASSPATH)
JCFLAGS=-cp $(CLASSPATH) -d "$(WD)/bin"
JDFLAGS=-d ./docs

.SUFFIXES: .java .class

default: build

all: clean build tests docs

run:
	@$(J) $(JFLAGS) ObjCreator 44240

runserver:
	@$(J) $(JFLAGS) Reciever 44240

build:
	@$(JC) $(JCFLAGS) $(wildcard ./src/*.java)
	@echo "Generated Java classes:"
	@ls ./bin/*.class | cat

tests:
	@$(JC) $(JCFLAGS) $(wildcard ./tests/*.java)
	@echo $(TESTS)
	@$(J) $(JFLAGS) org.junit.runner.JUnitCore SerializerTest

docs:
	@echo "Generating Documentation in ./docs/"
	@$(JD) $(JDFLAGS) $(wildcard ./src/*.java)

clean:
	@echo "Cleaning all Java class files..."
	@$(RM) ./bin/*.class