import java.io.File;
import java.io.IOException;
import org.junit.Test;
import static org.junit.Assert.*;

public class SerializerTest
{
    @Test
    public void testWrite()
    {
        Serializer s_one = new Serializer("file1.xml");
        Serializer s_two = new Serializer("file2.xml");

        try
        {
            s_one.write();
            s_two.write();

            File f_one = new File("file1.xml");
            File f_two = new File("file2.xml");

            assertTrue(f_one.exists());
            assertTrue(f_two.exists());

            assertTrue(f_one.delete());
            assertTrue(f_two.delete());
        }
        catch(IOException e)
        {
            fail("Encountered an IO Error");
        }
    }

    @Test
    public void testNull()
    {
        Serializer s = new Serializer();
        assertTrue(s.serialize(new Integer(5)) != null);
    }
}