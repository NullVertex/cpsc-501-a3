# CPSC 501 - Assignment 3
## Universal Java Serializer/Deserializer via XML
This tool allows you to serialize any object to a human-readable XML file  

1. Recursive serialization is working as intended
2. Deserializer is partially implemented
    - Can recreate objects but doesn't restore fields (yet)
2. Object creator is partially implemented
    - Just need to add a couple more options to the menu
4. Socket transmission and visualization
    - Make some sort of object comparator class?